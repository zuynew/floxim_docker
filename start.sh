#!/bin/bash



if [ ! -f /var/lib/mysql/ibdata1 ]; then
	mysql_install_db
fi

# Start MySQL and wait for it to become available
/usr/bin/mysqld_safe > /dev/null 2>&1 &

RET=1
while [[ $RET -ne 0 ]]; do
	echo "=> Waiting for confirmation of MySQL service startup"
	sleep 2
	mysql -uroot -e "status" > /dev/null 2>&1
	RET=$?
done

DB_EXISTS=0;

RESULT=`mysqlshow -uroot floxim| grep -v Wildcard | grep -o floxim`
if [ "$RESULT" == "floxim" ]; then
    DB_EXISTS=1;
fi

if [[ $DB_EXISTS -ne 1 ]]; then
	echo "=> Generating database and credentials"

	mysql -uroot -e "CREATE DATABASE floxim; GRANT ALL PRIVILEGES ON floxim.* TO 'floxim'@'localhost' IDENTIFIED BY '$FLOXIM_PASSWORD'; FLUSH PRIVILEGES;"
	mysql -uroot floxim < /var/www/floxim/install/floxim.sql

fi

mysqladmin -uroot shutdown

sed -i -e 's/<%FLOXIM_HOST_NAME%>/'"$FLOXIM_HOST_NAME"'/g' /etc/nginx/sites-available/default

sed -i -e 's/<%FLOXIM_HOST_NAME%>/'"$FLOXIM_HOST_NAME"'/g' /var/www/floxim/config.php
sed -i -e 's/<%FLOXIM_PASSWORD%>/'"$FLOXIM_PASSWORD"'/g' /var/www/floxim/config.php

cp -n -r /var/www/floxim/floxim_files/_content/* /var/www/floxim/floxim_files/content/

rm -rf /var/www/floxim/floxim_files/_content/

chown -R www-data:www-data /var/www/floxim

supervisord -n