<?php 
$config = array(
    'dev' =>  array(
        'db.name' => 'floxim',
        'db.host' => 'localhost',
        'db.user' => 'floxim',
        'db.password' => '<%FLOXIM_PASSWORD%>',
        'dev.on' => true,
        'templates.ttl' => 0
    )
);
return $config['dev'];