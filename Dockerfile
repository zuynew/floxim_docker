FROM    ubuntu:14.04
MAINTAINER Dmitry Zuev <zuynew@yandex.ru>

RUN \
	export LANG=C.UTF-8 && \
	apt-get -y install software-properties-common && \
	add-apt-repository ppa:ondrej/php5-5.6 && \
	add-apt-repository -y ppa:nginx/stable && \
	apt-get update && \
	apt-get -y install supervisor curl nginx php5 php5-fpm php5-mcrypt php5-curl php5-gd php5-mysql git mysql-client mysql-server


ENV FLOXIM_HOST_NAME floxim.loc
ENV FLOXIM_PASSWORD root
	
RUN \
	curl -s https://getcomposer.org/installer | php && \
	mv composer.phar /usr/local/bin/composer

RUN \
 	cd /var/www/ && \
 	git clone --depth 1 https://github.com/Floxim/floxim-demosite.git floxim && \
 	cd floxim && \
 	git submodule update --init && \
 	composer install

RUN mv /var/www/floxim/floxim_files/content /var/www/floxim/floxim_files/_content

CMD cd ~

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

ADD ./nginx-site.conf /etc/nginx/sites-available/default

ADD ./config.php /var/www/floxim/

ADD start.sh ./


RUN \
	sed -i -e "s/short_open_tag\s*=\s*Off/short_open_tag = On/g" /etc/php5/fpm/php.ini && \
	sed -i -e "s/;mbstring.func_overload\s*=\s*0/mbstring.func_overload = 7/g" /etc/php5/fpm/php.ini && \
	sed -i -e "s/pid\s*\/run\/nginx.pid;/pid \/var\/run\/nginx.pid;/g" /etc/nginx/nginx.conf && \
	sed -i '/pid \/var\/run\/nginx.pid;/a daemon off;' /etc/nginx/nginx.conf

# Disable SSH
RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


VOLUME ["/var/lib/mysql/"]

VOLUME ["/var/www/floxim/floxim_files/content"]

VOLUME ["/var/www/floxim/theme/My"]

EXPOSE 80

CMD ["/bin/bash", "/start.sh"]